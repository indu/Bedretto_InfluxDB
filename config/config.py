import os
from dotenv import load_dotenv


class INFLUXDB:
    load_dotenv()

    TOKEN=os.getenv('TOKEN')
    ORG = "org"
    URL = os.getenv('URL')
    BUCKET_WRITE = "test_geomonitor"
    BUCKET_READ = "tests_here"

class HydraulicConfig:
    BOREHOLES_METADATA_PATH = 'config/bedrettolab/boreholes.json'
    CONFIG_PATH = 'config/bedrettolab/geomonitor_config.json'
    WEBSERVICE = 'http://bedretto-hydws.ethz.ch:8080/hywds/v1'

    WATCH_DIRECTORY = '/Volumes/data/E2_hydraulics/'
    WATCH_WEBSERVICE = ''
    TIMEZONE = 'CH/CET'