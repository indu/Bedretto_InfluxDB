autopep8>=1.6.0
hydws-client @ git+https://git@gitlab.seismo.ethz.ch/indu/hydws-client.git
jinja2>=3.1.2
obspy>=1.3.0
pandas>=1.4.2
pyproj>=3.3.1
requests>=2.27.1
watchdog>=2.1.7
seismostats @ git+https://github.com/swiss-seismological-service/SeismoStats.git
flake8>=4.0.1
pytest>=7.1.2
pytest-cov>=3.0.0
pytest-dotenv>=0.5.2
influxdb-client

