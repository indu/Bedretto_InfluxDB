import pandas as pd
from influxdb_client import InfluxDBClient
from influxdb_client.client.write_api import SYNCHRONOUS
import os
from dotenv import load_dotenv

class INFLUXDB:
    load_dotenv()
    TOKEN=os.getenv('TOKEN')
    ORG = "org"
    URL = os.getenv('URL')
    BUCKET_WRITE = "test_geomonitor"
    BUCKET_READ = "tests_here"

def parse_InfluxDB_to_dataframe(
        query_api: InfluxDBClient.query_api,
        samplerate: str = '60s') -> pd.DataFrame:
    """
    Parses the content of a geomonitor file to a dataframe with the respective
    column names and a datetime index.

    :param query_api: query API for influxDBCLient
    :param samplerate: At which rate the file should be sampled.

    :return:    A datetime indexed dataframe. Empty columns are skipped and NaN
                values set to 0.
    """

    query="""
    from(bucket:"tests_here")
    |> range(start: -30m, stop: now())
    |> filter(fn: (r) => r._measurement == "flow_l_m")
    |> aggregateWindow(every: 1m, fn: last, createEmpty: false)
    |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
    |> keep(columns: ["_time","value"])
    """
    data = query_api.query_data_frame(query)
    data['datetime'] = pd.to_datetime(
        data['_time'],
        format='%Y-%m-%d %H:%M:%S.%f+00:00')
    data.set_index(['datetime'], inplace=True)
    data.drop(['_time','result','table'], axis=1, inplace=True)
    data.rename(columns={'value': 'inj_flow'}, inplace=True)

    return data


DBclient = InfluxDBClient(url=INFLUXDB.URL, token=INFLUXDB.TOKEN, org=INFLUXDB.ORG)

bucket = INFLUXDB.BUCKET_WRITE

write_api = DBclient.write_api(write_options=SYNCHRONOUS)
query_api = DBclient.query_api()

data = parse_InfluxDB_to_dataframe(query_api)