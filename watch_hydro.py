import logging

from config import HydraulicConfig

from datapipe import set_up_logger
from datapipe.parsers import parse_geomonitor_to_dataframe
from datapipe.watchers import FileWatcher
from datapipe.handlers import HydraulicsFileHandler

if __name__ == "__main__":
    set_up_logger()
    logger = logging.getLogger(__name__)

    handler = HydraulicsFileHandler(parse_geomonitor_to_dataframe)
    watcher = FileWatcher(handler, HydraulicConfig.WATCH_DIRECTORY)
    watcher.run()
