##SCHWEIZERISCHER ERDBEBENDIENST REAL TIME SYNC

The command noted in `SED_RT_SYNC.bat` copies over the contents of the specified directory to a central file storage.
Any changes to the flags must be thouroughly tested before being used in production.

For the script to function a Cygwin installation under `C:\cygwin64` is required and "rsync" must be added when choosing the packages to be installed alongside it.

The batch file is not executed directly but through `SED_RT_SYNC.vbs` which executes it without creating a cmd console window.

The execution of the vbs script can be scheduled via the Windows "Task Scheduler". For some reason it only works when selecting "Run only when user is logged on".
