import  time
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS
from config.config import INFLUXDB



DBclient = InfluxDBClient(url=INFLUXDB.URL, token=INFLUXDB.TOKEN, org=INFLUXDB.ORG)

bucket = INFLUXDB.BUCKET_WRITE

write_api = DBclient.write_api(write_options=SYNCHRONOUS)
query_api = DBclient.query_api()

for value in range(20):
    point = (
        Point("measurement1")
        .tag( "location","tag_value")
        .field("temperature", value)
    )
    write_api.write(bucket=bucket, org=INFLUXDB.ORG, record=point)
    time.sleep(1)  # separate points by 1 second

_point1 = Point("my_measurement").tag("location", "Prague").field("temperature", 25.3)
_point2 = Point("my_measurement").tag("location", "New York").field("temperature", 24.3)
write_api.write(bucket=bucket, record=[_point1, _point2])
data_frame = query_api.query_data_frame('from(bucket:"test_geomonitor") '
                                        '|> range(start: -10m) '
                                        '|> filter(fn: (r) => r._measurement == "my_measurement")'
                                        '|> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value") '
                                        '|> keep(columns: ["location", "temperature"])')

query = """from(bucket: "test_geomonitor")
 |> range(start: -10m)
 |> filter(fn: (r) => r._measurement == "measurement1")"""
tables = query_api.query(query, org=INFLUXDB.ORG)

query="""
from(bucket:"test_geomonitor")
|> range(start: -20m, stop: now())
|> filter(fn: (r) => r._measurement == "measurement1")
|> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
|> keep(columns: ["location", "temperature"])
"""
system_stats = query_api.query_data_frame('from(bucket:"test_geomonitor") '
                                        '|> range(start: -20m, stop: now())'
                                        '|> filter(fn: (r) => r._measurement == "measurement1")'
                                        '|> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value") '
                                        '|> keep(columns: ["temperature"])', org=INFLUXDB.ORG)

for table in tables:
  for record in table.records:
    print(record)
