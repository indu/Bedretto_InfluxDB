import io
import json
import logging
from abc import ABC, abstractmethod
from datetime import datetime, timedelta
from typing import Callable, Tuple

import pandas as pd
import requests
from hydws.parser.rawparser import RawHydraulicsParser

from config import HydraulicConfig
from datapipe import (delete_pickle, restore_pickles, save_pickle)\

from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS
from config.config import INFLUXDB

DBclient = InfluxDBClient(url=INFLUXDB.URL, token=INFLUXDB.TOKEN, org=INFLUXDB.ORG)

bucket_write = INFLUXDB.BUCKET_WRITE
bucket_read = INFLUXDB.BUCKET_READ

write_api = DBclient.write_api(write_options=SYNCHRONOUS)
query_api = DBclient.query_api()


class Handler(ABC):
    """
    The Handler is responsible for tying together the reading of the file(s),
    the parsing into the desired output format, keeping the state as well as
    eventually feeding the result into the desired service/DB.
    """

    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.file_dict = {}

        for file in restore_pickles():
            self.file_dict[file[0]] = {'time': datetime.now(),
                                       'data': file[1]
                                       }

    def update_past_data(self, file_name: str, data: pd.DataFrame):
        """
        Updates the entry in self.file_dict with new timestamp and line
        count. Also deletes keys older than 1 day.
        """
        # update key
        self.file_dict[file_name] = {'time': datetime.now(),
                                     'data': data
                                     }

        # create a pickle from it
        save_pickle(file_name, data)

        # delete keys and their pickles older than one day
        for k in list(self.file_dict.keys()):
            if self.file_dict[k]['time'] < datetime.now() - \
                    timedelta(days=1):
                del self.file_dict[k]
                try:
                    delete_pickle(k)
                except FileNotFoundError:
                    # file doesn't exist, no need to delete it
                    pass

    @abstractmethod
    def read(self, file: io.BytesIO):
        """
        Function which reads a new file and uploads the diff after parsing.

        :param file:    BytesIO object with the data and an attribute .name
                        of the file

        :returns:       None if no data uploaded, True if upload successful.
        """
        pass

    @abstractmethod
    def create_diff(self, filename: str, data: pd.DataFrame):
        pass

class HydraulicsFileHandler(Handler):
    """
    Handles Files, calculates their diffs since the last read, and uploads
    new content to a hydraulic webservice.

    :param file_parser: Function which transforms input file(s) to a dataframe.
    """

    def __init__(
            self,
            file_parser: Callable,
            memory: bool = True,
            check_changes: bool = False):
        super().__init__()
        self.file_parser = file_parser
        self.memory = memory

        self.check_changes = check_changes
        self.last_version = None

        with open(HydraulicConfig.BOREHOLES_METADATA_PATH, 'r') as f:
            boreholes_metadata = json.load(f)

        self.starttime = None
        self.endtime = None

        self.data_parser = RawHydraulicsParser(
            HydraulicConfig.CONFIG_PATH,
            boreholes_metadata)

    def read(self, file: io.BytesIO):

        # parse file to dataframe
        dataframe = self.file_parser(file)

        self.starttime = dataframe.index[0]
        self.endtime = dataframe.index[-1]

        if not self.has_changed(dataframe):
            return None

        # calculate the diff
        new_data = self.create_diff(file.name, dataframe)

        self.upload_data(new_data)

        # create HYDWS format
        #hydws_data = self.data_parser.parse(new_data)
        #print(hydws_data)

        #self.upload_data(hydws_data)

        if self.memory:
            self.update_past_data(file.name, dataframe)

    def has_changed(self, data: pd.DataFrame) -> bool:
        """
        Check if the data has changed since the last read.
        """
        if not self.check_changes:
            return True

        if self.last_version is None:
            self.last_version = data
            self.logger.info(
                'Checking changes enabled, first read of '
                'hydraulic data, wait for next read...')
            return False

        if data.equals(self.last_version):
            self.logger.info(
                'No changes in data, nothing updated.')
            return False
        self.logger.info('New data available.')
        self.last_version = data
        return True

    def create_diff(self, filename: str, data: pd.DataFrame) -> pd.DataFrame:

        if filename not in self.file_dict:
            return data

        new_lines = data.shape[0] - self.file_dict[filename]['data'].shape[0]

        if new_lines < 0:
            self.logger.error(
                'Hydraulic input data got smaller, skip updating of data.')
            new_lines = 0

        return data.tail(new_lines)

    def upload_data(self, data: pd.DataFrame):
        """ Upload data to the InfluxDB """
        self.logger.info('Upload of hydraulic data started.')

        write_api.write(INFLUXDB.BUCKET_WRITE, record = data, data_frame_measurement_name='geomonitor', data_frame_tag_columns=['bedretto'])

        self.logger.info('Upload of hydraulic data completed.')
