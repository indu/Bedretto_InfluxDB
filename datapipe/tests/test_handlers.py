from datetime import datetime, timedelta
import io

import requests
from unittest.mock import patch
from datapipe import QMLTemplate
from datapipe.parsers import parse_dugseis_to_dataframe, \
    parse_geomonitor_to_dataframe
from datapipe.handlers import HydraulicsFileHandler, SeismicsFileHandler
from watchdog import events
from datapipe.watchers import WatchFileOrDirectoryHandler


@patch('datapipe.handlers.call_scdispatch')
def test_seismic_handler(mock_scdispatch):
    qml_template = QMLTemplate('datapipe/templates/template.xml')
    handler = SeismicsFileHandler(parse_dugseis_to_dataframe, qml_template)

    with open('datapipe/tests/example_data/events_1.csv', 'rb') as f1:
        handler.update_past_data('events_1.csv',
                                 handler.file_parser(f1))

    with open('datapipe/tests/example_data/events_2.csv', 'rb') as f2:
        myfile = io.BytesIO(f2.read())
        myfile.name = 'events_1.csv'
        handler.read(myfile)


@patch('datapipe.handlers.requests.post')
def test_hydraulic_handler(mock_post):
    # Setup
    mock_post.return_value.ok = True
    mock_post.return_value.text = 'Return text from mock response'
    event1 = events.FileSystemEvent('datapipe/tests/example_data/DAT-part.DAT')
    event2 = events.FileSystemEvent(
        'datapipe/tests/example_data/DAT-part2.DAT')

    handler = HydraulicsFileHandler(parse_geomonitor_to_dataframe)
    watcher = WatchFileOrDirectoryHandler(handler)

    # run the whole function, no error should occur
    watcher.on_any_event(event1)

    # simulate that the next file is the same as before:
    handler.file_dict[event2.src_path] = handler.file_dict[event1.src_path]

    # check filediff
    with open(event2.src_path, 'rb') as f:
        new_df = handler.file_parser(f)
        diff_df = handler.create_diff(event2.src_path, new_df)
        assert diff_df.shape[0] == 1

    # test exception handling:
    event_dir = events.DirModifiedEvent('datapipe/tests/example_data/')

    mock_post.side_effect = requests.ConnectionError()
    assert watcher.on_any_event(event1) is None
    mock_post.side_effect = None

    # test if events older than 2 days are deleted
    handler.file_dict[event2.src_path]['time'] = datetime.now() - \
        timedelta(days=3)
    handler.update_past_data(event_dir.src_path, new_df)

    assert event2.src_path not in handler.file_dict
