import os
from datapipe import QMLTemplate


def test_template():
    template_gen = QMLTemplate('datapipe/tests/example_data/template.xml')
    filename = 'test.xml'
    my_dict = {
        'originid': 'origin/p_wave_travel_time/homogeneous_model/e6547f47-e5c7'
        '-422b-845f-0d8b51592914',
        'time': '2022-02-09T14:54:18.730410Z',
        'lon': 8.48569468323252,
        'lat': 46.5140000004569,
        'depth': -1917.49752283449,
        'mag': '',
        'Mw': '',
        'class': 'passive',
        'eventid': 'event/0a7d6731-2640-4f34-930f-69690fde39ac'}
    template_gen.create_qml(my_dict, filename)

    assert os.path.isfile(filename)

    with open('datapipe/tests/example_data/test.xml', 'r') as f:
        reference = f.read()
    with open('test.xml', 'r') as f:
        new_file = f.read()
    assert new_file == reference
    os.remove(filename)
