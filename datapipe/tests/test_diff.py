from datapipe.parsers import parse_dugseis_to_dataframe
from datapipe import compare_dataframes


def test_diff():
    with open('datapipe/tests/example_data/events_1.csv', 'rb') as e1:
        events_1 = parse_dugseis_to_dataframe(e1)

    with open('datapipe/tests/example_data/events_2.csv', 'rb') as e2:
        events_2 = parse_dugseis_to_dataframe(e2)

    new, changed, deleted = compare_dataframes(events_2, events_1)

    assert len(new) == 499 and 'eventid' in new.columns
    assert len(changed) == 12 and 'eventid' in changed.columns
    assert len(deleted) == 1 and 'eventid' in deleted.columns
