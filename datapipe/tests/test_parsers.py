import pandas as pd
from numpy.testing import assert_almost_equal

from datapipe.parsers import parse_geomonitor_to_dataframe


def test_geomonitor_parser():
    with open('datapipe/tests/example_data/DAT-209.DAT',
              encoding="ISO-8859-1") as f:
        df = parse_geomonitor_to_dataframe(f)

    # ignore gaps in data
    assert_almost_equal(df['CB2-1'].iloc[-1], 2.5116478, decimal=5)

    # do not read empty columns
    assert 'CB3' not in df.columns
    assert 'T13' not in df.columns

    # exists twice, only one version
    assert 'FM8' in df.columns and 'FM8.1' not in df.columns

    # datetime index correctly set
    isinstance(df.index, pd.DatetimeIndex)

    # datetime correctly parsed
    assert df.index[0].day == 5
    assert df.index[0].year == 2021
    assert df.index[0].hour == 0

    # no negative values
    assert not (df < 0).values.any()

    # check resampling
    assert df.shape[0] == 649
    assert (df.index[1] - df.index[0]).total_seconds() == 60

    with open('datapipe/tests/example_data/DAT-209.DAT',
              encoding="ISO-8859-1") as f:
        df2 = parse_geomonitor_to_dataframe(f, '120s')

    assert df2.shape[0] == 325
