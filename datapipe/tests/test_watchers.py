from datapipe.watchers import FileWatcher
from datapipe.parsers import parse_geomonitor_to_dataframe
from datapipe.handlers import HydraulicsFileHandler


def test_watcher_init():
    handler = HydraulicsFileHandler(parse_geomonitor_to_dataframe)
    watcher = FileWatcher('.', handler)  # noqa
