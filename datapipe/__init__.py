import base64
import logging
import os
import subprocess
from logging.handlers import TimedRotatingFileHandler
from pathlib import Path
from subprocess import CalledProcessError
from typing import List, Tuple

import numpy as np
import pandas as pd
from jinja2 import Template, select_autoescape

LOGGER = logging.getLogger(__name__)


def set_up_logger(foldername: str = 'logs'):
    os.makedirs(foldername, exist_ok=True)
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s - %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
        handlers=[
            TimedRotatingFileHandler(
                f'{foldername}/datapipe.log',
                when='d',
                interval=1,
                backupCount=5),
            logging.StreamHandler()
        ]
    )


def save_pickle(name: str, data: pd.DataFrame, dir: str = './tmp') -> None:
    """
    Encodes the name as b64 and saves the DataFrame as *name*.pkl
    """
    os.makedirs(dir, exist_ok=True)

    # absolute_filepath = str(Path(filename).resolve().name)

    encoded_filename = base64.b64encode(
        name.encode('ascii')).decode('ascii')
    pickle_name = os.path.join(dir, f'{encoded_filename}.pkl')
    data.to_pickle(pickle_name)


def restore_pickles(dir: str = './tmp') -> List[Tuple[str, pd.DataFrame]]:
    """
    Reads all the *.pkl files in the directory and returns a list of
    "name, dataframe" tuples where the name is the decoded b64 filename.
    """
    data = []
    for pickle_file in Path(dir).glob('*.pkl'):
        decoded_filename = base64.b64decode(
            pickle_file.stem.encode('ascii')).decode('ascii')
        data.append((decoded_filename, pd.read_pickle(pickle_file)))

    return data


def delete_pickle(name: str, dir: str = './tmp') -> None:
    encoded_name = base64.b64encode(
        name.encode('ascii')).decode('ascii')
    try:
        Path(os.path.join(dir, f'{encoded_name}.pkl')).unlink()
    except FileNotFoundError:
        LOGGER.error('Could not delete Pickle file. File not Found.')
        raise

def compare_dataframes(new: pd.DataFrame,
                       old: pd.DataFrame) -> Tuple[pd.DataFrame,
                                                   pd.DataFrame,
                                                   pd.DataFrame]:
    """
    Compare two dataframes and calculate the data which is new,
    which changed and which got deleted. Based on a unique index column
    """

    # get all deleted events
    del_indexes = np.setdiff1d(old.index.values, new.index.values)
    deleted_data = old.loc[del_indexes, :].copy()
    old_diff = old.drop(index=del_indexes)

    # get all new events
    new_indexes = np.setdiff1d(new.index.values, old.index.values)
    new_data = new.loc[new_indexes, :].copy()
    new_diff = new.drop(index=new_indexes)

    # sort indexes for comparison
    old_diff.sort_index(axis=0, inplace=True)
    old_diff.sort_index(axis=1, inplace=True)
    new_diff.sort_index(axis=0, inplace=True)
    new_diff.sort_index(axis=1, inplace=True)

    # get all changed data
    changed_data = new.loc[old_diff.compare(new_diff).index, :].copy()

    # data cleaning
    for df in (new_data, changed_data, deleted_data):
        df.select_dtypes(exclude='number').fillna('', inplace=True)

    return new_data, changed_data, deleted_data
