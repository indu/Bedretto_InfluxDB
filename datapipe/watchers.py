import io
import logging
import os
import time
from typing import List
from urllib.parse import urlparse

import urllib3
from watchdog.events import FileSystemEvent, PatternMatchingEventHandler
from watchdog.observers.polling import PollingObserver as Observer

from datapipe.handlers import Handler


class WatchFileOrDirectoryHandler(PatternMatchingEventHandler):
    def __init__(self,
                 file_handler: Handler,
                 pattern: List[str] = ['*']):

        super().__init__(pattern)
        self.logger = logging.getLogger(__name__)
        self.file_handler = file_handler

    def on_any_event(self, event: FileSystemEvent):
        """
        Function which is triggered on every change in the target directory.

        :param event: Triggered event
        """
        if event.is_directory:
            return None

        logging.info(event)

        with open(event.src_path, 'rb') as f:
            file = io.BytesIO(f.read())
            file.name = event.src_path
        try:
            self.file_handler.read(file)
        except Exception as e:
            self.logger.error(e, exc_info=True)


class FileWatcher:
    def __init__(self,
                 file_handler: Handler,
                 directory_path: str,
                 absolute_file_paths: List[str] = ['*'],
                 polling_interval=1):

        self.logger = logging.getLogger(__name__)
        self.observer = None
        self.polling_interval = polling_interval
        self.event_handler = WatchFileOrDirectoryHandler(file_handler,
                                                         absolute_file_paths)
        self.directory = directory_path

    def recover(self):
        """ Remove the current observer and try starting a new one. """

        self.logger.error(f"Directory {self.directory} "
                          "lost, trying to recover...")
        self.observer.stop()
        self.observer.join()
        self.logger.info("Watcher Terminated")
        self.run()

    def run(self):
        """ Start an observer in case that the directory exists. """
        try:
            while not os.path.isdir(self.directory):
                self.logger.warning(
                    f'Directory {self.directory} could not be found '
                    'and will not be watched.')
                time.sleep(10)

            self.observer = Observer(timeout=self.polling_interval)
            self.observer.schedule(
                self.event_handler, self.directory, recursive=False)
            self.observer.start()

            self.logger.info(f"Watcher Running in {self.directory}")

            while True:
                time.sleep(5)
                if not os.path.isdir(self.directory):
                    return self.recover()

        except KeyboardInterrupt:
            self.logger.info("Watcher terminated by user.")

        finally:
            self.observer.stop()
            self.observer.join()
            self.logger.info("Watcher Terminated")

class WebWatcher:
    def __init__(self,
                 file_handler: Handler,
                 file_url: str,
                 poll_interval: int = 30):

        self.file_handler = file_handler
        self.poll_interval = poll_interval
        self.file_url = file_url

        self.logger = logging.getLogger(__name__)
        self.http = urllib3.PoolManager()

    def run(self):
        try:
            while True:
                with self.http.request('GET', self.file_url,
                                       preload_content=False) as r:
                    if not r.status == 200:
                        self.logger.warning('Could not find file: '
                                            f'{self.file_url}')
                    else:
                        filename = urlparse(r._request_url).path.split('/')[-1]
                        file = io.BytesIO(r.data)
                        file.name = filename
                        self.file_handler.read(file)

                time.sleep(self.poll_interval)
        except KeyboardInterrupt:
            self.logger.info("Watcher terminated by user.")
        except Exception as e:
            logging.error(e, exc_info=True)
            time.sleep(self.poll_interval)
            self.run()
